Write a function that returns `True` if a sequence of arbitrary integers are
in a list of integers:

find_in_sequence([1, 3, 5], [2, 3, 4, 1, 3, 5, 7, 9, 1, 3, 5])  # True
find_in_sequence([1, 3, 5], [2, 3, 4, 1, 3, 5, 9, 7, 1, 3, 6])  # False
