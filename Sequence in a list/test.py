#!/usr/bin/env python3

import random
import timeit


#
# When I first saw this test, my brain jumped to a simple solution involving
# loops & comparisons, but after a little more thought, I came up with what I
# think is a crazy-brilliant-simple solution involving casting.  Finally, It
# occurred to me that this is the sort of thing that is probably a solved
# problem out there, so I looked it up online where I found a third solution.
#
# I'm going to show all three here and then benchmark them against different
# data sets.  I have no idea which method will win at this point, which makes
# it kinda exciting :-)
#

#
# [Goes away to write the code]
#

#
# I honestly didn't expect this, but here's the results on my machine:
#
#                     short:common  long:common  short:rare  long:rare
# ------------------------------------------------------------------------
# simple_loop         0.035         0.763        0.034       38.592
# crazy_idea          0.012         10.260       0.014       12.589
# knuth_morris_pratt  0.040         0.497        0.039       40.186
#
# The Knuth-Morris-Pratt algorithm performs the worst of the three, which I
# suppose serves as a lesson to those who just copy/paste stuff from the
# internet without benchmarking first.  Still, this is an adaptation of a
# method initially designed to yield a list of all occurrences, so it's not
# exactly a fair fight.
#
# My simple_loop() method did pretty well, especially in cases where the
# sequence you're looking for is likely to be early in the list, as it exits
# early on success.  On large lists with a low chance of the sequence
# appearing, this method can get *really* slow though.
#
# My crazy idea of casting the list as a string and doing basic string
# comparisons turned out pretty well.  This is probably due to the fact that
# these comparisons are done in C rather than executing everything in a bunch
# of Python loops.  However it falls down in cases where we're dealing with
# cases where it's likely that the sequence is in there, especially if it
# occurs early in the list.
#
# The takeaway:
#
# 1. Crazy ideas pay off, and this one might make sense depending on the
#    use-case.  Obviously not for the long:common case, but it was clearly
#    superior in the three other categories.
#
# 2. If performance is a concern, bench test stuff you find on the internet
#    before blindly using it.
#
#
# This test was fun.  A lot of tests out there do their level best to screen
# for developers who can code *quickly* rather than creatively or well, so I
# appreciate your efforts in this regard.
#
# I hope I didn't miss any test cases, but if I have, I'd appreciate a
# heads-up.  I tend to keep code like this around for reference, and I'd hate
# to be archiving faulty code.
#


class SequenceFinder:

    BENCH_MAP = {
        "short": 100,
        "long": 100000,
        "common": 10,
        "rare": 10000
    }

    def __init__(self):

        self.methods = (
            self.simple_loop,
            self.crazy_idea,
            self.knuth_morris_pratt
        )

    @staticmethod
    def simple_loop(needle, haystack):
        """
        Loop over haystack and look for our sequence sequentially.  Exit early
        if we find it.
        """

        if not needle:
            return True

        needle_length = len(needle)
        for h_index, i in enumerate(haystack):

            for n_index, n in enumerate(needle):

                try:
                    next_value = haystack[h_index + n_index]
                except IndexError:
                    break

                if next_value != n:
                    break

                # If we're at the end of our subsequence, return with a win.
                if n_index + 1 == needle_length:
                    return True

        return False

    @staticmethod
    def crazy_idea(needle, haystack):
        """
        Cast both arguments as strings and do simple string comparisons to find
        our needle.
        """

        if not needle:
            return True

        stringy_needle = str(needle).replace(" ", "")[1:-1]
        stringy_haystack = str(haystack).replace(" ", "")[1:-1]

        if stringy_needle in stringy_haystack:
            if stringy_needle == stringy_haystack:
                return True
            if "," + stringy_needle + "," in stringy_haystack:
                return True
            if stringy_haystack.startswith(stringy_needle):
                if stringy_needle + "," in stringy_haystack:
                    return True
            if stringy_haystack.endswith(stringy_needle):
                if "," + stringy_needle in stringy_haystack:
                    return True

        return False

    @staticmethod
    def knuth_morris_pratt(needle, haystack):
        """
        Copied shamelessly from http://code.activestate.com/recipes/117214/
        The truth is, I'm not a math person, and would never have known about
        Knuth-Morris-Pratt, so this solution is here simply as a way of showing
        that I know when to find someone who knows better than me and to not do
        the work myself.
        """

        if not needle:
            return True

        # allow indexing into pattern and protect against change during yield
        needle = list(needle)

        # build table of shift amounts
        shifts = [1] * (len(needle) + 1)
        shift = 1
        for pos in range(len(needle)):
            while shift <= pos and needle[pos] != needle[pos-shift]:
                shift += shifts[pos-shift]
            shifts[pos+1] = shift

        # do the actual search
        start_pos = 0
        match_len = 0
        for c in haystack:
            while match_len == len(needle) or \
                  match_len >= 0 and needle[match_len] != c:
                start_pos += shifts[match_len]
                match_len -= shifts[match_len]
            match_len += 1
            if match_len == len(needle):
                return True

        return False

    def run_tests(self):

        test_list = [
            1, 2, 3, 4, 5, 7, 11, 2, 3, 4, 1, 23, 4, 67, 8, 1, 2, 34, 5, 6, 7]
        pass_list = [[1, 2], [2, 3, 4, 5], [5, 6], []]
        fail_list = [[0, 1, 2], [7, 8], [9], [3, 2, 1]]

        for pass_test in pass_list:
            for method in self.methods:
                print("Testing {} with {} against {}".format(
                    method.__name__, pass_test, test_list))
                assert method(pass_test, test_list)
                assert method(tuple(pass_test), test_list)

        for fail_test in fail_list:
            for method in self.methods:
                print("Testing {} with {} against {}".format(
                    method.__name__, fail_test, test_list))
                assert not method(fail_test, test_list)

    def run_benchmarks(self, times=1000):

        print("Running benchmarks")

        template = "{:<18}  {:<12}  {:<11}  {:<10}  {:<9}"
        results = {}

        bench_names = (
            "short:common", "long:common", "short:rare", "long:rare")

        print(template.format("", *bench_names))

        for method in self.methods:

            method_name = method.__name__
            results[method_name] = []

            for name in bench_names:

                test = "SequenceFinder.{}([1, 3, 4], {})".format(
                    method_name, self.get_bench_list(name))
                setup = "from __main__ import SequenceFinder"

                results[method_name].append("{:0.3f}".format(timeit.timeit(
                    test, setup=setup, number=times)))

            print(template.format(method_name, *results[method_name]))

    def get_bench_list(self, name):
        """
        Re-generate the list of random numbers for each test using values in
        self.BENCH_MAP to dictate the nature of "short", "long", "common", and
        "rare".
        """

        length = self.BENCH_MAP[name.split(":")[0]]
        scarcity = self.BENCH_MAP[name.split(":")[1]]

        return [random.randint(0, scarcity) for _ in range(length)]


if __name__ == "__main__":
    finder = SequenceFinder()
    finder.run_tests()
    finder.run_benchmarks()
