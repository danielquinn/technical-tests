from django import forms


class BootstrapMixin:
    """
    A simple way to have Django generate form fields with bootstrappy classes
    attached to them.
    """

    # Don't apply `form-control` to these fields
    UNCONTROLLED_FIELDS = (
        forms.widgets.RadioSelect,
        forms.widgets.CheckboxInput,
        forms.widgets.CheckboxSelectMultiple,
        forms.widgets.FileInput
    )

    def _bootstrapify(self):

        for field_name in self.fields.keys():

            widget = self.fields[field_name].widget

            self.fields[field_name].label_suffix = ""
            if not isinstance(widget, self.UNCONTROLLED_FIELDS):
                widget.attrs.update({"class": "form-control"})
