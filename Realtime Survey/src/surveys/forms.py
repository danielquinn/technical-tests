"""
Yeah I know there's a lot of copypasta here, but my thinking here is that using
inheritance to programmatically set these values would make things more complex
and less readable.  Besides, these sorts of things usually start out simple and
grow into big & complex.  Best to keep things easy from the start.
"""

from django import forms
from django.forms.widgets import CheckboxSelectMultiple, RadioSelect
from hotmess.forms import BootstrapMixin

from .models import Survey


class SurveyFormStage1(forms.ModelForm, BootstrapMixin):

    class Meta:
        model = Survey
        fields = ("name", "email")

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self._bootstrapify()
        self.instance.stage1_completed = True
        for field in ("name", "email"):
            self.fields[field].required = True


class SurveyFormStage2(forms.ModelForm, BootstrapMixin):

    class Meta:
        model = Survey
        fields = ("age", "about")

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self._bootstrapify()
        self.instance.stage2_completed = True
        for field in ("age", "about"):
            self.fields[field].required = True


class SurveyFormStage3(forms.ModelForm, BootstrapMixin):

    class Meta:
        model = Survey
        fields = ("address", "gender")

    gender = forms.ChoiceField(choices=Survey.GENDERS, widget=RadioSelect)

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self._bootstrapify()
        self.instance.stage3_completed = True


class SurveyFormStage4(forms.ModelForm, BootstrapMixin):

    class Meta:
        model = Survey
        fields = ("book", "colours")

    colours = forms.MultipleChoiceField(
        widget=CheckboxSelectMultiple, choices=Survey.COLOURS, required=False)

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self._bootstrapify()
        self.instance.stage4_completed = True
