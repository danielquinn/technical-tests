from unittest.mock import Mock, patch

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.test import TestCase, Client

from ..views import (
    SurveyStage1View,
    SurveyStage2View,
    SurveyStage3View,
    SurveyStage4View,
    SurveyView
)
from .factories import SurveyFactory


class SurveyViewTestCase(TestCase):

    def test_dispatch_bounce_n00bs_to_stage_1(self):
        view = SurveyView()
        view.STAGE = 2
        response = view.dispatch(Mock(session={}))
        self.assertIsInstance(response, HttpResponseRedirect)
        self.assertEqual(response["Location"], reverse("surveys:stage-1"))

    @patch("surveys.views.FormView.dispatch", return_value="ok")
    def test_dispatch_allow_n00bs_to_stage_1(self, *args):
        view = SurveyView()
        view.STAGE = 1
        self.assertEqual(view.dispatch(Mock(session={}, META={})), "ok")

    def test_dispatch_redirects_previous_survey_stage_without_referrer(self):

        view = SurveyView()
        view.STAGE = 3
        view._check_previous_completed = Mock(return_value=False)
        view._check_this_completed = Mock(return_value=False)

        survey = SurveyFactory.create(stage2_completed=False)
        request = Mock(session={"survey": survey.pk}, META={}, method="GET")
        response = view.dispatch(request)

        self.assertEqual(response["Location"], reverse("surveys:stage-2"))
        self.assertEqual(view._survey, survey)

    def test_dispatch_redirects_next_survey_stage_without_referrer(self):

        view = SurveyView()
        view.STAGE = 3
        view._check_previous_completed = Mock(return_value=True)
        view._check_this_completed = Mock(return_value=True)

        survey = SurveyFactory.create(stage3_completed=True)
        request = Mock(session={"survey": survey.pk}, META={}, method="GET")
        response = view.dispatch(request)

        self.assertEqual(response["Location"], reverse("surveys:stage-4"))
        self.assertEqual(view._survey, survey)

    @patch("surveys.views.FormView.dispatch", return_value="ok")
    def test_dispatch_does_not_redirect_when_moving_backward(self, *args):

        view = SurveyView()
        view.STAGE = 3
        view._check_previous_completed = Mock(return_value=True)
        view._check_this_completed = Mock(return_value=True)

        survey = SurveyFactory.create()
        request = Mock(
            session={"survey": survey.pk},
            META={"HTTP_REFERER": reverse("surveys:stage-4")}
        )

        response = view.dispatch(request)
        self.assertEqual(response, "ok")
        self.assertEqual(view._survey, survey)

    @patch("surveys.views.FormView.dispatch", return_value="ok")
    def test_dispatch_does_not_redirect_when_moving_forward(self, *args):

        view = SurveyView()
        view.STAGE = 3
        view._check_previous_completed = Mock(return_value=True)
        view._check_this_completed = Mock(return_value=True)

        survey = SurveyFactory.create()
        request = Mock(
            session={"survey": survey.pk},
            META={"HTTP_REFERER": reverse("surveys:stage-2")}
        )
        response = view.dispatch(request)

        self.assertEqual(response, "ok")
        self.assertEqual(view._survey, survey)

    @patch("surveys.views.SurveyView._get_previous_url", return_value="p")
    @patch("surveys.views.SurveyView._get_next_url", return_value="n")
    def test_dispatch_redirects_for_completed_surveys(self, *args):

        survey = SurveyFactory.create(
            stage1_completed=True,
            stage2_completed=True,
            stage3_completed=True,
            stage4_completed=True,
        )
        request = Mock(session={"survey": survey.pk}, META={})
        for stage in range(1, 5):
            view = SurveyView()
            view.STAGE = stage
            self.assertEqual(view.dispatch(request).status_code, 302)

    @patch("surveys.views.FormView.get_form_kwargs", return_value={"x": "y"})
    def test_get_form_kwargs(self, *args):
        view = SurveyView()
        view._survey = "survey"
        self.assertEqual(view.get_form_kwargs(), {
            "instance": "survey",
            "x": "y"
        })

    @patch("surveys.views.FormView.get_context_data", return_value={"x": "y"})
    def test_get_context_data_without_previous(self, *args):
        view = SurveyView()
        view.STAGE = 1
        self.assertEqual(view.get_context_data(), {"x": "y"})

    @patch("surveys.views.FormView.get_context_data", return_value={"x": "y"})
    @patch("surveys.views.SurveyView._get_previous_url", return_value="url")
    def test_get_context_data_with_previous(self, *args):
        for stage in range(2, 5):
            view = SurveyView()
            view.STAGE = stage
            self.assertEqual(
                view.get_context_data(),
                {"previous": "url", "x": "y"}
            )

    @patch("surveys.views.FormView.form_valid", return_value="ok")
    @patch("surveys.views.Group")
    def test_form_valid(self, m_group, *args):

        survey = SurveyFactory.create()
        request = Mock(session={})
        form = Mock(save=Mock(return_value=survey))

        m_test = Mock()
        m_group.return_value = Mock(send=m_test)

        view = SurveyView()
        view.request = request

        self.assertEqual(view.form_valid(form), "ok")
        self.assertEqual(view._survey, survey)
        self.assertEqual(view.request.session["survey"], survey.pk)
        self.assertEqual(m_test.call_count, 1)


class SurveyStage1ViewTestCase(TestCase):

    def test__check_this_completed_incomplete(self):
        view = SurveyStage1View()
        view._survey = SurveyFactory.create(stage1_completed=False)
        self.assertFalse(view._check_this_completed())

    def test__check_this_completed_complete(self):
        view = SurveyStage1View()
        view._survey = SurveyFactory.create(stage1_completed=True)
        self.assertTrue(view._check_this_completed())

    def test__check_previous_completed(self):
        view = SurveyStage1View()
        view._survey = SurveyFactory.create()
        self.assertTrue(view._check_previous_completed())


class SurveyStage2ViewTestCase(TestCase):

    def test__check_this_completed_incomplete(self):
        view = SurveyStage2View()
        view._survey = SurveyFactory.create(stage2_completed=False)
        self.assertFalse(view._check_this_completed())

    def test__check_this_completed_complete(self):
        view = SurveyStage2View()
        view._survey = SurveyFactory.create(stage2_completed=True)
        self.assertTrue(view._check_this_completed())

    def test__check_previous_completed_incomplete_stage1(self):
        view = SurveyStage2View()
        view._survey = SurveyFactory.create(stage1_completed=False)
        self.assertFalse(view._check_this_completed())

    def test__check_previous_completed_complete_stage1(self):
        view = SurveyStage2View()
        view._survey = SurveyFactory.create(stage1_completed=True)
        self.assertTrue(view._check_previous_completed())


class SurveyStage3ViewTestCase(TestCase):

    def test__check_this_completed_incomplete(self):
        view = SurveyStage3View()
        view._survey = SurveyFactory.create(stage3_completed=False)
        self.assertFalse(view._check_this_completed())

    def test__check_this_completed_complete(self):
        view = SurveyStage3View()
        view._survey = SurveyFactory.create(stage3_completed=True)
        self.assertTrue(view._check_this_completed())

    def test__check_previous_completed_incomplete_stage2(self):
        view = SurveyStage3View()
        view._survey = SurveyFactory.create(stage2_completed=False)
        self.assertFalse(view._check_this_completed())

    def test__check_previous_completed_complete_stage2(self):
        view = SurveyStage3View()
        view._survey = SurveyFactory.create(stage2_completed=True)
        self.assertTrue(view._check_previous_completed())


class SurveyStage4ViewTestCase(TestCase):

    def test__check_this_completed_incomplete(self):
        view = SurveyStage4View()
        view._survey = SurveyFactory.create(stage4_completed=False)
        self.assertFalse(view._check_this_completed())

    def test__check_this_completed_complete(self):
        view = SurveyStage4View()
        view._survey = SurveyFactory.create(stage4_completed=True)
        self.assertTrue(view._check_this_completed())

    def test__check_previous_completed_incomplete_stage3(self):
        view = SurveyStage4View()
        view._survey = SurveyFactory.create(stage3_completed=False)
        self.assertFalse(view._check_this_completed())

    def test__check_previous_completed_complete_stage3(self):
        view = SurveyStage4View()
        view._survey = SurveyFactory.create(stage3_completed=True)
        self.assertTrue(view._check_previous_completed())


class SurveyViewIntegrationTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_visit_stage_1_without_session(self):
        self.assertEqual(
            self.client.get(reverse("surveys:stage-1")).status_code, 200)

    def test_visit_stage_2_without_session(self):
        response = self.client.get(reverse("surveys:stage-2"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["Location"], reverse("surveys:stage-1"))

    def test_visit_stage_3_without_session(self):
        response = self.client.get(reverse("surveys:stage-3"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["Location"], reverse("surveys:stage-1"))

    def test_visit_stage_4_without_session(self):
        response = self.client.get(reverse("surveys:stage-4"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["Location"], reverse("surveys:stage-1"))

    def test_visit_stage2_before_stage1_complete(self):
        self._set_session(
            "survey", SurveyFactory.create(stage1_completed=False).pk)
        response = self.client.get(reverse("surveys:stage-2"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["Location"], reverse("surveys:stage-1"))

    def test_visit_stage3_before_stage2_complete(self):
        self._set_session(
            "survey", SurveyFactory.create(stage2_completed=False).pk)
        response = self.client.get(reverse("surveys:stage-3"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["Location"], reverse("surveys:stage-2"))

    def test_visit_stage1_after_stage1_complete(self):
        self._set_session(
            "survey", SurveyFactory.create(stage1_completed=True).pk)
        response = self.client.get(reverse("surveys:stage-1"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["Location"], reverse("surveys:stage-2"))

    def test_visit_stage1_from_stage2_after_stage1_complete(self):
        self._set_session(
            "survey", SurveyFactory.create(stage1_completed=True).pk)
        referrer = 'https://example.com{}'.format(reverse("surveys:stage-2"))
        response = self.client.get(
            reverse("surveys:stage-1"), HTTP_REFERER=referrer)
        self.assertEqual(response.status_code, 200)

    def test_visit_stage_2_after_survey_complete(self):
        self._set_session(
            "survey",
            SurveyFactory.create(
                stage1_completed=True,
                stage2_completed=True,
                stage3_completed=True,
                stage4_completed=True
            ).pk
        )
        response = self.client.get(reverse("surveys:stage-1"), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.redirect_chain,
            [(reverse("surveys:thanks"), 302)]
        )

    def _set_session(self, key, value):
        session = self.client.session
        session[key] = value
        session.save()
