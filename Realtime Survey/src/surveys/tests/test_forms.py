from django.test import TestCase

from ..forms import SurveyFormStage1, SurveyFormStage2


class SurveyFormStage1TestCase(TestCase):

    def test___init__(self):
        form = SurveyFormStage1()
        self.assertTrue(form.fields["name"].required)
        self.assertTrue(form.fields["email"].required)

    def test_is_valid_pass(self):
        form = SurveyFormStage1({"name": "Name", "email": "test@test.ca"})
        self.assertTrue(form.is_valid())

    def test_is_valid_fail_missing_name(self):
        form = SurveyFormStage1({"email": "test@test.ca"})
        self.assertFalse(form.is_valid())
        self.assertIn("name", form.errors)

    def test_is_valid_fail_missing_email(self):
        form = SurveyFormStage1({"name": "Name"})
        self.assertFalse(form.is_valid())
        self.assertIn("email", form.errors)

    def test_is_valid_fail_bad_email(self):
        form = SurveyFormStage1({"name": "Name", "email": "test"})
        self.assertFalse(form.is_valid())
        self.assertIn("email", form.errors)


class SurveyFormStage2TestCase(TestCase):

    def test___init__(self):
        form = SurveyFormStage2()
        self.assertTrue(form.fields["age"].required)
        self.assertTrue(form.fields["about"].required)

    def test_is_valid_pass(self):
        form = SurveyFormStage2({"age": "38", "about": "test"})
        self.assertTrue(form.is_valid())

    def test_is_valid_fail_missing_age(self):
        form = SurveyFormStage2({"about": "test"})
        self.assertFalse(form.is_valid())
        self.assertIn("age", form.errors)

    def test_is_valid_fail_missing_about(self):
        form = SurveyFormStage2({"age": "38"})
        self.assertFalse(form.is_valid())
        self.assertIn("about", form.errors)

    def test_is_valid_fail_bad_age(self):
        form = SurveyFormStage2({"age": "thirtyeight", "about": "test"})
        self.assertFalse(form.is_valid())
        self.assertIn("age", form.errors)
