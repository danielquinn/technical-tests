from django.test import TestCase

from .factories import SurveyFactory


class SurveyTestCase(TestCase):

    def test_is_completed(self):
        survey = SurveyFactory.create()
        self.assertFalse(survey.is_completed)

        survey.stage1_completed = True
        self.assertFalse(survey.is_completed)

        survey.stage2_completed = True
        self.assertFalse(survey.is_completed)

        survey.stage3_completed = True
        self.assertFalse(survey.is_completed)

        survey.stage4_completed = True
        self.assertTrue(survey.is_completed)
