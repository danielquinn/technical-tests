from collections import Counter
import json

from django.contrib import admin

from .models import Survey


class SurveyAdmin(admin.ModelAdmin):

    class Media:
        js = (
            "contrib/js/d3.min.js",
            "contrib/js/c3.min.js",
            "admin/js/surveys-changelist.js",
        )
        css = {
            "all": ("contrib/css/c3.min.css",)
        }

    list_display = (
        "name", "email", "age", "about", "address", "gender_", "book",
        "colours", "is_completed"
    )

    def gender_(self, obj):
        if obj.gender == "?":
            return ""
        return obj.get_gender_display()

    def changelist_view(self, request, extra_context=None):
        """
        Attach statistical data to the admin page
        """
        jkwargs = {"separators": (",", ":")}
        return admin.ModelAdmin.changelist_view(
            self,
            request,
            extra_context={
                "ages": json.dumps(self._get_ages(), **jkwargs),
                "genders": json.dumps(self._get_genders(), **jkwargs),
                "colours": json.dumps(self._get_colours(), **jkwargs)
            }
        )

    def _get_ages(self):
        return list(Survey.objects.completed().values_list("age", flat=True))

    def _get_genders(self):
        return list(
            Survey.objects.completed().values_list("gender", flat=True)
        )

    def _get_colours(self):
        colours = Counter()
        for survey in Survey.objects.completed():
            for colour in survey.colours:
                colours[colour] += 1
        return dict(colours)


admin.site.register(Survey, SurveyAdmin)
