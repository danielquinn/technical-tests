from channels import Group

from .settings import CHANNEL_UPDATES


def socket_connect(message):
    message.reply_channel.send({"accept": True})
    Group(CHANNEL_UPDATES).add(message.reply_channel)


def socket_disconnect(message):
    Group(CHANNEL_UPDATES).discard(message.reply_channel)
