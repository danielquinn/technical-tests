#!/usr/bin/env python3


def reverse_string(string):
    return "".join(reversed(list(string)))


assert reverse_string("abcdefgh") == "hgfedcba"
