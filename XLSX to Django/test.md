You were pretty explicit about not building a proper application so I hope a
standard markdown document will do.  I've included code snippets where I thought
it was appropriate, and outlines my reasoning in the text as well as in code
docstrings.

The problem seems simple enough: you have non-nerds managing a "database" in the
form of an excel spreadsheet and you want to bring them into the fold of your
existing Django infrastructure.

As with all situations with an absence of detail (for example, there were no
instructions as to the nature of the interface, only that one needed to exist,
and there's no means for me to acquire additional detail at this stage), I'm
inclined to go the simplest route.  This (a) makes the job easier, so the client
(you) can see what I'm thinking sooner, and (b) means that if what I've done is
not what you wanted, the cost of throwing everything out is minimal.

So, you want a CRUD interface and you're using a Django stack?  The smartest
option from where I'm sitting is the standard Django Admin.  It's customisable
for our purposes (group permissions etc) and if your environment is anything
like most Django projects, you're probably already using the Admin extensively.

I'll cover three components here that should nicely explain how I'd do things:

1. The data model
2. The importer
3. The permissions

## The Data Model

Other than the spreadsheet, there's not much to go on regarding the existing
state of the database.  Without that, I'm having to make some assumptions
regarding the database layout.  There's no mention of anything like change
tracking, device ownership, or related data in the system, so I've done my best
to guess at the right structure.  If things in the existing system differ, this
model may require changing to better take advantage of existing structure.

```python
import re

from django.db import models
from django_extensions.db.fields import AutoSlugField


class SlugModel(models.Model):
    """
    I like slugs.  I think it's good practise to tag most models with a slug if
    it's likely you'll ever do things like list a bunch of "X" that belong to
    category "Y".  They can also be handy in an API where users may be more
    comfortable using slugs when filtering, so you get a URL that looks like
    this:

      /path/to/resources?type=cake

    as opposed to this:

      /path/to/resources?type=3

    Usually, I like to support both types in an API filter.
    """

    name = models.CharField(max_length=64, unique=True)
    slug = AutoSlugField(populate_from="name")

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Type(SlugModel):
    pass


class Location(SlugModel):
    """
    This really should be a more structured layout, best done with a `City`
    class that has a `country` property that's provided by a standard like
    django-countries, but at present the data is just too free-form and I have
    no means of cleaning it up without further understanding the reasons why
    the location is defined so loosely.  I mean, what exactly is someone
    supposed to do with locations like "California/London", "?", and "Jim's
    House" anyway?
    """
    pass


class Network(SlugModel):
    pass


class Device(models.Model):

    ACTIVE = 1
    RETIRED = 2
    UNKNOWN = 3
    DEPLOYMENT_STATES = (
        (ACTIVE, "Active"),
        (RETIRED, "Retired"),
        (UNKNOWN, "Unknown"),
    )

    make = models.CharField(max_length=64, blank=True)
    serial = models.CharField(max_length=32, blank=True)
    type = models.ForeignKey(Type, blank=True, null=True)
    location = models.ForeignKey(Location, blank=True, null=True)
    mac_address = models.CharField(
        max_length=12, blank=True,
        help_text="The MAC address, without any colons, spaces, or hyphens.  "
                  "Only the letters and numbers."
    )
    network = models.ForeignKey(Network, blank=True, null=True)
    notes = models.TextField(blank=True)
    removed = models.DateField(blank=True, null=True)
    last_checked = models.DateField(blank=True, null=True)
    deployment_state = models.PositiveIntegerField(choices=DEPLOYMENT_STATES)

    def save(self, *args, **kwargs):
        """
        Force conformation on MAC addresses
        """

        self.mac_address = re.sub(r"[^a-f0-9]*", "", self.mac_address.lower())

        models.Model.save(self, *args, **kwargs)
```

### Naming

Naming is super important and since a lot of people tend to glaze over it, I'd
like to bring special attention to it.

The app should be called `devices`, plural as-per Django standards.  The primary
model here is `Device`, singular since an instantiation of this class
constitutes a single device.  I've opted for `Type` instead of something like
`DeviceType` because redundant redundancy is redundant.  Python also allows for
the convention where one might `import Type as DeviceType` in cases where more
than one `Type` class may be imported.

I'm also not fond of how I've named `removed`.  It could be `removed_at` or
`removed_on`, or even left as-is, but so long as it's not `removed_date`, I
guess I'm happy.


### What's With the Stubby Models?

In the absence of additional information about the existing data structure, I'm
going with the assumption that these stub-models are likely stand-ins for
full-sized models in the existing database.


### Conforming the MAC Address

I hate messy data, and MAC addresses are very uniform.  There's no reason that I
can see that justifies storing these as free-form text fields allowing users to
do all kinds of crazy things, so I strip everything down to the bare minimum: 12
hex characters.


## The Importer

Unsurprisingly, the data quality of a system maintained by passing a spreadsheet
around is pretty bad.  There's missing data, differently-spellt data, data in
the wrong columns, you name it, it's in there.  Typically, my go-to solution for
these sorts of problems is to have a sit-down with the people responsible and
try to clear up some of the confusion, but given that this isn't an option in
this case, I'm going to do my best.

What we're doing here is essentially a data migration, but since it depends on
an external file and we only intend to run this once, I opted to do this as a
management command instead:

```python
import csv
import os
import re

from dateutil import parser as dateparser

from django.core.management.base import BaseCommand, CommandError

from ...models import Device, Type, Location, Network


class Command(BaseCommand):
    """
    Import an unfriendly .csv into a structured database.  Note that this code
    doesn't account for stripping off the first line of column headers, so do
    that first before running this thing.
    """

    def __init__(self):

        BaseCommand.__init__(self)

        # Lookup tables to keep us from re-querying the db for stuff we should
        # know already
        self._types = {}
        self._locations = {}
        self._networks = {}

        # Make deployment state lookups cleaner
        self.deployment_states = {
            "active": Device.ACTIVE,
            "retired": Device.RETIRED
        }

    def add_arguments(self, parser):
        parser.add_argument("inventory-file", type=str)

    def handle(self, *args, **options):

        inventory = options["inventory-file"]
        if not os.path.exists(inventory):
            raise CommandError("File does not exist: {}".format(inventory))

        with open(inventory) as f:
            reader = csv.reader(f, delimiter=',', quotechar='"')
            for row in reader:
                row = self._prewash(row)
                Device.objects.create(
                    make=self._clean_make(row[0]),
                    serial=self._clean_serial(row[1]),
                    type=self._clean_type(row[2]),
                    location=self._clean_location(row[3]),
                    mac_address=self._clean_mac_address(row[4]),
                    network=self._clean_network(row[5]),
                    notes=self._clean_notes(row[6]),
                    removed=self._clean_removed(row[7]),
                    last_checked=self._clean_last_checked(row[8]),
                    deployment_state=self._clean_deployment_state(row[9])
                )

    def _prewash(self, row):
        """
        Some of the data is really mangled, and things need to be moved around
        before we start importing the rows.
        """

        # The `removed` column appears to be polluted with what should be
        # free-form notes, so we figure out what-goes-where by attempting to
        # parse the field as a date.  If it fails, we append it to the notes
        # field and hope for the best.
        try:
            dateparser.parse(row[7])
        except ValueError:
            row[6], row[7] = "{} {}".format(row[6], row[7]), ""

        return [_.strip() for _ in row]

    def _clean_make(self, string):
        return self._generic_clean_simple(string)

    def _clean_serial(self, string):
        return self._generic_clean_simple(string)

    def _clean_type(self, string):
        return self._generic_clean_foreign_key("types", Type, string)

    def _clean_location(self, string):
        return self._generic_clean_foreign_key("locations", Location, string)

    def _clean_mac_address(self, string):
        """
        Since MAC addresses have a defined format, there's no need to store the
        colons or worry about text case.  For the sake of uniformity, conform
        all the things.

        For some reason, there appears to be a few things that look like MAC
        addresses but definitely aren't.  In these cases, we just warn the user
        and drop the string.
        """

        r = re.sub(
            r"[^a-f0-9]*", "", self._generic_clean_simple(string).lower())

        if len(r) > 12:
            print("* WARNING: This doesn't look like a MAC: {}".format(string))
            return ""

        return r

    def _clean_network(self, string):

        # Account for inconsistent spelling
        if string == "BES10":
            string = "BES 10"

        return self._generic_clean_foreign_key("networks", Network, string)

    def _clean_notes(self, string):
        return self._generic_clean_simple(string)

    def _clean_removed(self, string):
        return self._generic_clean_date(string)

    def _clean_last_checked(self, string):
        return self._generic_clean_date(string)

    def _clean_deployment_state(self, string):
        try:
            return self.deployment_states[
                self._generic_clean_simple(string).lower()]
        except KeyError:
            return Device.UNKNOWN

    @staticmethod
    def _generic_clean_simple(string):
        if string.lower() in ("unknown", "n/a"):
            return ""
        return string

    def _generic_clean_foreign_key(self, index, klass, string):

        string = self._generic_clean_simple(string)

        if not string:
            return None

        key = string.lower()
        lookup = getattr(self, "_{}".format(index))
        try:
            return lookup[key]
        except KeyError:
            lookup[key] = klass.objects.create(name=string)
            return lookup[key]

    def _generic_clean_date(self, string):

        string = self._generic_clean_simple(string)

        if not string:
            return None

        try:
            return dateparser.parse(string)
        except ValueError:
            print("* WARNING: This doesn't look like a date: {}".format(string))
            return None
```

## The Permissions

Django allows you to do all manner of crazy things in the admin, including
specifying your own form with custom logic for each field if you like, but given
the single requirement of field-specific permissions, I don't think that any of
that should be necessary at this point.  It looks like all we'd need is:

* Django's existing permissions structure, including its groups framework
* The `readonly_fields` attribute on the Admin class.

```python
from django.contrib import admin

from .models import Device, Type, Location, Network


class DeviceAdmin(admin.ModelAdmin):
    list_display = (
        "make",
        "serial",
        "type",
        "location",
        "removed",
        "deployment_state",
    )
    list_filter = (
        "deployment_state",
        "removed",
        "last_checked",
        "network",
        "location",
    )

    def get_readonly_fields(self, request, obj=None):
        """
        If the user is in the "IT Support" group, there's no restriction on
        editing the fields, but everyone else (including the "Scheduling" group)
        are limited to only editing the location.

        Django has further permissions powers in this area, including
        model-level permissions, but since the only requirement we have to go on
        here is this distinction between the two permissions groups, that's all
        that's implemented.
        """

        if request.user.is_superuser:
            return []

        if request.user.groups.filter(name="IT Support").exists():
            return []

        return [_.name for _ in Device._meta.fields if not _.name == "location"]

admin.site.register(Device, DeviceAdmin)
admin.site.register(Type)
admin.site.register(Location)
admin.site.register(Network)
```

## Notes

The original PDF had four notes appended to the bottom, and I wanted to go over
them:

1. **You do not need to build a full working application**: I hope this wasn't
   too much code.  I tried to keep the bits provided relevant.
2. **Make sure you document any thoughts you have regarding performance, making
   the app accessible from 3rd-party web applications, etc.**:  Strictly
   speaking, making the data available to 3rd-party applications wasn't part of
   the exercise so I didn't detail it above.  With that said though,
   [Django REST Framework](http://www.django-rest-framework.org/) is the
   industry standard tool for this these days, and given the current simplicity
   of the data structure, it would be a small amount of work to setup a ViewSet,
   some filters, and a few serialisers for what we have here.
3. **You should not spend more than 4 hours on this task.**: I'm coming up on
   the end of 4 hours on this Saturday afternoon as I write this.  I may pretty
   it up a bit though before I post it.
4. **The company web application has an "Employee" model. You can use this in
   your app.**: The only time I needed to reference a user was by way of the
   `request` object.  If your system is making use of Django's newer "write
   your own user model" then I guess I did use the `Employee` model, but if it's
   still using the "contrib user + user profile" model, then I guess I never
   needed to use it.

## Conclusion

This was fun, thanks for issuing a test that wasn't the typical:

> Write a function that returns the 180th prime number multiplied by the nth
> Fibinacci number.
 
It's nice to see practical real-life use cases applied in the interview process,
as it gives the impression that you care about building software that works for
real people.

