# Sort a Chaotic CSV into a Proper Database

The management board have decided that they want to replace the company IT
inventory spreadsheet with a Django application that will need to be
integrated into the company back-office application - also written in Django.

IT support have provided you with the attached spreadsheet. This is the way
they currently track IT inventory.

* Design a Django app that stores the IT inventory information and allows
  users to perform basic CRUD actions.
* How would you import the existing data?
* How can you ensure that Read/Write access is restricted by user role:
    * "IT support" role can do anything
    * "Scheduling" role have read access to all fields but can only edit the
      Location

## Notes

1. You do not need to build a full working application. Instead, demonstrate
   your thought processes and include the most important bits of code that
   would be required for this app to work.
2. Make sure you document any thoughts you have regarding performance, making
   the app accessible from 3rd party web applications, etc.
3. You should not spend more than 4 hours on this task. Again: We don’t need
   you to build a full working application, just the most essential bits of
   code that demonstrate how you would approach the problem.
4. The company web application has an “Employee” model. You can use this in
   your app.

