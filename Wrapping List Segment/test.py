#!/usr/bin/env python

def take(items: list, total: int):
    return list(generator(items, total))[:total]


def generator(items: list, total: int):
    length = len(items)
    while total > 0:
        total -= length
        for i in items:
            yield i


assert list(take(["apple", "pear", "lemon", "orange"], 5)) == ["apple", "pear", "lemon", "orange", "apple"]
