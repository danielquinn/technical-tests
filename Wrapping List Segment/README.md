# Wrapping List Segments

## Part 1

Write a function that takes a list and an integer and returns a list with up to
the n-th element in the list. In case of the integer being bigger than the size
of the list, return the original list.

Example:

```python
take(["apple", "pear", "lemon", "orange"], 2)
# ["apple", "pear"]
```

## Part 2

Imagine the list was circular. So in the above example, apple would follow
orange:

```python
take(["apple", "pear", "lemon", "orange"], 5)
# ["apple", "pear", "lemon", "orange", "apple"]
```
