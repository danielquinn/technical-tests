from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.static import serve

from rest_framework import routers

from images.views import ExpiringImageView, ImageViewSet


router = routers.DefaultRouter()
router.register(r"images", ImageViewSet)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/1/", include((router.urls, "api-1"))),
    path(
        "expiring-images/<uuid:uuid>/",
        ExpiringImageView.as_view(),
        name="expiring-images",
    ),
]

# https://docs.djangoproject.com/en/3.0/ref/views/#serving-files-in-development

if settings.DEBUG:  # pragma: no cover
    urlpatterns += [
        re_path(
            r"^media/(?P<path>.*)$",
            serve,
            {"document_root": settings.MEDIA_ROOT},
        ),
    ]
