from io import BytesIO
from pathlib import Path
from uuid import uuid4

from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import models

from PIL import Image as PillowImage


class ThumbnailManager(models.manager.Manager):
    def create_from_image(self, image, size, **kwargs):
        suffix = Path(image.original.path).suffix

        buffer = BytesIO()
        with PillowImage.open(image.original.path) as im:
            # This is an obvious hack, but it's based on missing information
            # in the README as we're only provided the preferred height.  The
            # assumption is that we want to preserve the aspect ratio, but
            # without a specification for maximum width, all we can do is allow
            # for any reasonable (for some definition of "reasonable") width.
            #
            # Pillow's API for thumbnailing (the easiest way to preserve aspect
            # ratio) is to provide both a width and height, so without a value
            # for maximum width, opting for 99999 seems like the best option as
            # it ensures that in pretty much every case, the height will be the
            # deciding factor in the resize.
            im.thumbnail((99999, size.height))
            im.save(buffer, format=image.mimetype.split("/")[1])

            self.create(
                base=image,
                size=size,
                file=SimpleUploadedFile(
                    name=f"{uuid4()}.{suffix}",
                    content=buffer.getvalue(),
                    content_type=image.mimetype,
                ),
                **kwargs,
            )
