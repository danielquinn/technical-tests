from pathlib import Path
from typing import Type
from uuid import uuid4

from django.db import models

from .managers import ThumbnailManager


def get_image_path(instance: Type["Image"] | Type["Thumbnail"], *args) -> str:
    if isinstance(instance, Image):
        section = "originals"
        mimetype = instance.mimetype
    else:
        section = "thumbnails"
        mimetype = instance.base.mimetype

    suffix = {
        "image/png": "png",
        "image/jpeg": "jpg",
    }[mimetype]

    return (Path("images") / section / f"{instance.uuid}.{suffix}").as_posix()


class Image(models.Model):
    uuid = models.UUIDField(default=uuid4)
    original = models.ImageField(upload_to=get_image_path)
    mimetype = models.CharField(max_length=64)
    creator = models.ForeignKey("users.User", on_delete=models.CASCADE)

    def save(self, *args, **kwargs) -> None:
        """
        "Why not signals?" you might ask.  Because that's the class bad
        use-case for something like this.  Signals are meant as a way for your
        application to communicated with libraries you don't control, the
        classic case o hooking into user login events for example: you don't
        control the login code (that's in Django), so tapping into signals is
        necessary.

        For code you control though, using signals just obfuscates what your
        code is actually doing, which violates the sacred law of "principle of
        least surprise".  Overriding `.save()` however is explicit, which makes
        figuring out what your code is doing easier.

        This is a hill I will die on.  Careless use of signals has burned me
        too many times.
        """

        from .tasks.thumbnailer import generate_thumbnails

        super().save(*args, **kwargs)

        for size in self.creator.tier.sizes.all():
            generate_thumbnails.delay(self.pk, size.pk)


class Thumbnail(models.Model):
    uuid = models.UUIDField(default=uuid4)
    base = models.ForeignKey(
        Image,
        on_delete=models.CASCADE,
        related_name="thumbnails",
    )
    size = models.ForeignKey(
        "users.Size",
        on_delete=models.CASCADE,
        related_name="thumbnails",
    )
    file = models.ImageField(upload_to=get_image_path)

    objects = ThumbnailManager()
