from typing import IO, Any

from django.core.files.base import File
from django.http import Http404
from django.shortcuts import get_object_or_404

from magic import from_buffer as get_mime_from_buffer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from users.models import Size

from .models import Image, Thumbnail
from .services import ExpiringLinkService


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ("name", "height")


class ThumbnailSerializer(serializers.ModelSerializer):
    size = SizeSerializer()

    class Meta:
        model = Thumbnail
        fields = ("uuid", "size", "file")


class ImageWriteSerializer(serializers.ModelSerializer):
    ACCEPTABLE_MIMETYPES = ("image/png", "image/jpeg")

    original = serializers.ImageField()

    class Meta:
        model = Image
        fields = ("original",)

    def validate_original(self, attr: File) -> File:
        if self._get_mimetype(attr) not in self.ACCEPTABLE_MIMETYPES:
            raise ValidationError("Invalid file type.")
        return attr

    @staticmethod
    def _get_mimetype(file: IO[bytes]) -> str:
        """
        Get the mime type by reading the first 2KB out of the file, as the MIME
        header data is likely in there.
        """
        header = file.read(2048)
        file.seek(0)
        return get_mime_from_buffer(header, mime=True)

    def validate(self, attrs: dict[str, Any]) -> dict[str, Any]:
        validated_data = super().validate(attrs)

        mimetype = self._get_mimetype(validated_data["original"])
        if mimetype not in self.ACCEPTABLE_MIMETYPES:
            raise ValidationError("Invalid file type.")

        validated_data["creator"] = self.context["request"].user
        validated_data["mimetype"] = mimetype

        return validated_data


class ImageReadSerializer(serializers.ModelSerializer):
    uuid = serializers.CharField()
    original = serializers.ImageField()
    thumbnails = ThumbnailSerializer(
        source="thumbnail_list",
        read_only=True,
        many=True,
    )

    class Meta:
        model = Image
        fields = (
            "uuid",
            "original",
            "thumbnails",
        )

    def get_fields(self) -> list[str]:
        """
        Since DRF does a "helpful" check that all fields defined on the class
        are in fact referenced in `Meta.fields`, we have to get a little
        creative here and *remove* values from the list of fields rather than
        the more intuitive appending you'd expect.
        """

        r = super().get_fields()

        for field_name in self._get_fields_to_be_deleted():
            del r[field_name]

        return r

    def _get_fields_to_be_deleted(self):
        user = self.context["request"].user

        if not user.is_authenticated:
            return ["original", "expiring_url"]

        r = []
        if not user.tier.can_see_original:
            r.append("original")

        return r


class ExpiringImageSerializer(serializers.Serializer):
    expires = serializers.IntegerField(min_value=300, max_value=30000)

    def update(self, instance, validated_data):  # pragma: no cover
        raise NotImplementedError()

    def create(self, validated_data):
        return ExpiringLinkService.get_url(
            self.context["request"],
            str(self.context["view"].kwargs["uuid"]),
            int(validated_data["expires"]),
        )

    def validate(self, attrs) -> dict[str, Any]:
        """
        We're raising Http404() here instead of a proper ValidationError
        because doing the latter would leak the fact that the UUID in question
        exists in the first place.
        """
        image = get_object_or_404(
            Image, uuid=self.context["view"].kwargs["uuid"]
        )
        user = self.context["request"].user

        if not user == image.creator:
            raise Http404()

        if not user.tier.can_use_expiring_urls:
            raise Http404()

        return attrs
