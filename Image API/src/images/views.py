from pathlib import Path

from django.db.models import Prefetch
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from rest_framework.exceptions import ValidationError
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import (
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
)
from rest_framework.permissions import (
    IsAuthenticated,
    IsAuthenticatedOrReadOnly,
)
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.viewsets import GenericViewSet

from .models import Image, Thumbnail
from .serializers import (
    ExpiringImageSerializer,
    ImageReadSerializer,
    ImageWriteSerializer,
)
from .services import ExpiringLinkService


class ImageViewSet(
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    GenericViewSet,
):
    queryset = Image.objects
    permission_classes = (IsAuthenticated,)
    http_method_names = ("post", "get")
    lookup_field = "uuid"

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .prefetch_related(
                Prefetch(
                    "thumbnails",
                    queryset=Thumbnail.objects.filter(
                        size__in=self.request.user.tier.sizes.all()
                    ),
                    to_attr="thumbnail_list",
                ),
            )
            .filter(creator=self.request.user)
        )

    def get_serializer_class(self):
        if self.request.method == "POST":
            return ImageWriteSerializer
        return ImageReadSerializer


class ExpiringImageView(GenericAPIView):
    queryset = Image.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = ExpiringImageSerializer
    http_method_names = ("post", "get")
    lookup_field = "uuid"

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.instance, status=HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        try:
            expiry = int(self.request.GET["expiry"])
            signature = self.request.GET["signature"]
        except (KeyError, TypeError):
            raise ValidationError()

        is_verified = ExpiringLinkService.verify(
            kwargs["uuid"],
            expiry,
            signature,
        )
        if not is_verified:
            raise ValidationError("The URL does not appear to be valid.")

        image = get_object_or_404(Image, uuid=self.kwargs["uuid"])
        with Path(image.original.path).open("rb") as f:
            return HttpResponse(f.read(), content_type=image.mimetype)
