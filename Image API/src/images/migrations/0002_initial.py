# Generated by Django 4.1.7 on 2023-02-25 21:36

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("users", "0001_initial"),
        ("images", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="thumbnail",
            name="size",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="thumbnails",
                to="users.size",
            ),
        ),
        migrations.AddField(
            model_name="image",
            name="creator",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
