from datetime import datetime, timedelta
from hashlib import sha512
from urllib.parse import urlencode

from django.conf import settings
from django.http import HttpRequest
from django.urls import reverse


class ExpiringLinkService:
    # It's reusing a value meant for something else, but since this value is
    # also used for defining basic things like sessions, I figure it's in the
    # same neighbourhood.
    SECRET = settings.SECRET_KEY

    @classmethod
    def sign(cls, uuid: str, expiry: int) -> str:
        return sha512(f"{cls.SECRET}:{expiry}:{uuid}".encode()).hexdigest()

    @classmethod
    def verify(cls, uuid: str, expiry: int, signature: str) -> bool:
        if cls.sign(uuid, expiry) == signature:
            if datetime.fromtimestamp(expiry) > datetime.now():
                return True
        return False

    @classmethod
    def get_url(cls, request: HttpRequest, uuid: str, expires: int) -> str:
        expiry = int((datetime.now() + timedelta(seconds=expires)).timestamp())
        signature = cls.sign(uuid=uuid, expiry=expiry)
        return request.build_absolute_uri(
            "{path}?{qs}".format(
                path=reverse("expiring-images", kwargs={"uuid": uuid}),
                qs=urlencode({"signature": signature, "expiry": expiry}),
            )
        )
