from django.test import TestCase
from django.urls import reverse

from freezegun import freeze_time
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
)

from users.tests.factories import TierFactory, UserFactory

from .factories import ImageFactory
from .helpers.files import JPG


class ExpiringImagesTestCase(TestCase):
    """
    Ability to fetch a link that expires after a number of seconds (user can
    specify any number between 300 and 30000).
    """

    def test_expire_time_outside_window(self):
        # Arrange
        user = UserFactory(tier=TierFactory(can_use_expiring_urls=True))
        image = ImageFactory(creator=user)
        self.client.force_login(user)

        # Act
        url = reverse("expiring-images", kwargs={"uuid": str(image.uuid)})
        response = self.client.post(url, data={"expires": 999999})

        # Assert
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)

    def test_cannot_create_links_without_rights(self):
        # Arrange
        tier = TierFactory(can_use_expiring_urls=False)
        user = UserFactory(tier=tier)
        image = ImageFactory(creator=user)
        self.client.force_login(user)

        # Act
        url = reverse("expiring-images", kwargs={"uuid": str(image.uuid)})
        response = self.client.post(url, data={"expires": 300})

        # Assert
        self.assertEqual(response.status_code, HTTP_404_NOT_FOUND)

    def test_cannot_create_links_to_other_user_images(self):
        # Arrange
        tier = TierFactory(can_use_expiring_urls=True)
        user_a = UserFactory(tier=tier)
        user_b = UserFactory(tier=tier)
        image = ImageFactory(creator=user_a)
        self.client.force_login(user_b)

        # Act
        url = reverse("expiring-images", kwargs={"uuid": str(image.uuid)})
        response = self.client.post(url, data={"expires": 300})

        # Assert
        self.assertEqual(response.status_code, HTTP_404_NOT_FOUND)

    def test_happy_path(self):
        # Arrange
        tier = TierFactory(can_use_expiring_urls=True)
        user = UserFactory(tier=tier)
        image = ImageFactory(creator=user)
        self.client.force_login(user)

        with freeze_time("2023-01-01"):
            # Act
            url = reverse("expiring-images", kwargs={"uuid": str(image.uuid)})
            response = self.client.post(url, data={"expires": 300})

            # Assert
            self.assertEqual(response.status_code, HTTP_201_CREATED)

            url = response.json()
            self.assertTrue(url.startswith("http://testserver/"))
            self.client.logout()

            response = self.client.get(url)
            self.assertEqual(response.status_code, HTTP_200_OK)
            self.assertEqual(response.content, JPG)

        # Check that outside the window the URL fails
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
