from django.test import TestCase
from django.urls import reverse

from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST

from users.tests.factories import SizeFactory, TierFactory, UserFactory

from ..models import Image, Thumbnail
from .helpers.files import get_gif, get_jpg


class UploadTestCase(TestCase):
    """
    Users should be able to upload images via HTTP request.
    """

    def test_bad_file(self):
        # Arrange
        self._login()

        # Act
        response = self.client.post(
            reverse("api-1:image-list"),
            data={"original": get_gif()},
        )

        # Assert
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertEqual(Image.objects.count(), 0)
        self.assertEqual(Thumbnail.objects.count(), 0)

    def test_happy_path(self):
        # Arrange
        self._login()

        # Act
        response = self.client.post(
            reverse("api-1:image-list"),
            data={"original": get_jpg()},
        )

        # Assert
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(Image.objects.count(), 1)
        self.assertEqual(Thumbnail.objects.count(), 2)

    def _login(self):
        tier = TierFactory()
        tier.sizes.add(SizeFactory(height=1))
        tier.sizes.add(SizeFactory(height=2))
        user = UserFactory(tier=tier)
        self.client.force_login(user)
