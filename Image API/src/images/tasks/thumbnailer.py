from django.db.transaction import atomic

from django_rq import job

from users.models import Size

from ..models import Image, Thumbnail


@job
def generate_thumbnails(image_id: int, size_id: int):
    with atomic():
        thumbnails = Thumbnail.objects.filter(
            base_id=image_id,
            size_id=size_id,
        )
        for thumbnail in thumbnails:
            thumbnail.delete()

        Thumbnail.objects.create_from_image(
            Image.objects.get(pk=image_id),
            Size.objects.get(pk=size_id),
        )
