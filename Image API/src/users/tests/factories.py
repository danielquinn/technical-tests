import factory

from ..models import Size, Tier, User


class SizeFactory(factory.django.DjangoModelFactory):
    name = "Test"
    height = 200

    class Meta:
        model = Size


class TierFactory(factory.django.DjangoModelFactory):
    name = "Test"

    class Meta:
        model = Tier


class UserFactory(factory.django.DjangoModelFactory):
    email = factory.Faker("email")
    name = "Test"
    tier = factory.SubFactory(TierFactory)

    class Meta:
        model = User
