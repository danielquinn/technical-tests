from django.contrib import admin
from django.db.models.query import QuerySet
from django.http import HttpRequest

from .models import Size, Tier, User


@admin.register(Size)
class SizeAdmin(admin.ModelAdmin):
    list_display = ("name", "height")


@admin.register(Tier)
class TierAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("name", "tier", "is_staff", "is_superuser")

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return super().get_queryset(request).select_related("tier")
