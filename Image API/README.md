# ImageCruncher

Using Django REST Framework, write an API that allows any user to upload an
image in PNG or JPG format.

You are allowed to use any libraries or base projects / cookie cutters you want
(but using DRF is a hard requirement).

Skip the registration part, assume users are created via the admin panel.


## Requirements

* It should be possible to easily run the project. Docker-compose is a plus.
* Users should be able to upload images via HTTP request
* Users should be able to list their images
* There are three account tiers: Basic, Premium and Enterprise:
    * Users that have "Basic" plan after uploading an image get:
        * A link to a thumbnail that's 200px in height
    * Users that have "Premium" plan get:
        * A link to a thumbnail that's 200px in height
        * A link to a thumbnail that's 400px in height
        * A link to the originally uploaded image
    * Users that have "Enterprise" plan get
        * A link to a thumbnail that's 200px in height
        * A link to a thumbnail that's 400px in height
        * A link to the originally uploaded image
        * Ability to fetch a link that expires after a number of seconds (user
          can specify any number between 300 and 30000)
* Admins should be able to create arbitrary plans with the following things
  configurable:
    * Thumbnail sizes
    * Presence of the link to the originally uploaded file
    * Ability to generate expiring links
* Admin UI should be done via django-admin
* There should be no custom user UI (just browsable API from DRF)
* Remember about:
    * Tests
    * Validation
    * Performance considerations (assume there can be a lot of images and the
      API is frequently accessed)


## Running it

It's built on Docker with compose, so standing it up is one command:

```shell
$ docker-compose up
```

This will bring up a few containers:

* `db`: The PostgreSQL database
* `redis`: Used for caching & queueing
* `web`: The Django webserver
* `worker`: The rq-based worker
* `setup`: A short-running process to run the migrations and a few other things
  to get the initial start into a state that's easy to play with.  Details
  around what it's doing and why we do it this way can be found in
  `scripts/setup`.


### Tests

I didn't get it up to 100% coverage as I've already spent enough time on this.
96% isn't too bad though.  The tests are triggered from within the container,
so to run them from outside you just:

```shell
$ docker-compose exec web /app/scripts/test
```

This is a test wrapper script that allows you to supply a few nice flags like
`--coverage` for getting coverage checking, `--report` for generating an HTML
coverage report, and `--migrations` to run a follow-up check for missing
migrations.

There's also a script at `/app/scripts/check-styles` that will run `black`,
`isort`, and `flake8` against the codebase.  It's nice to combine this with
testing in the CI.


## Using it

The admin stuff should be obvious and self-explanatory, but in order to do
anything, you'll need to log in.  Go to [admin](http://localhost:8000/admin/)
first to authenticate with username: `test@test.ca` and password ` ` (that's
just a space).  Once you're authenticated you can check out the API endpoints
which are a little cryptic, so it's worth explaining them.

The `/api/1/images/` endpoint is for creating and listing images.  This is a
`ModelViewSet`, so DRF handles it nicely, rendering it into the root API view.
The expiring images however are handled via a normal `APIView`, and are
therefore not rendered into the fancy view by DRF.  You just have to know where
it is: `/expiring-images/<uuid-of-the-image>`.

So, if you've stood up everything with docker-compose, then you just have to
point your browser [here](http://localhost:8000/expiring-images/00000000-0000-0000-0000-000000000000/)
to play with the expiring images.


## Developer Notes

### Gross Time Estimation

There is no way that anyone can do the above to any measurable degree of
quality within the prescribed 2 to 6 hour estimate.  Using these numbers sends
a few potential signals to prospective candidates:

1. The company regularly expects impossible turn around times on large, complex
   tasks.
2. No one at the company knows how long this much work actually takes.
3. No one cares about either of the above.

Projects like this aren't even a good gauge of one's ability to do this work as
much of the past 10 hours or so has been a process of digging up code samples
of when I did `x` a few years ago, or sifting through StackOverflow to remind
me how to do `y`.  There's nothing particularly interesting/impressive in here,
so that leaves the candidate feeling like this is some sort of loyalty test to
prove how much you want the job rather than a means for one to demonstrate
their skill.

If you're at all interested in my recommendation, I suggest that in the future,
rather than asking someone to give up their weekend to write throwaway code for
you, you might instead hold a 1hr coding session and pair with them on a
(small) technical problem.  I've found this works really well for both sides of
the interview, and you get the added bonus of seeing how the candidate deals
with feedback and criticism.


### "After Uploading"

The spec says that users should get their various perks "after uploading an
image", but since you don't generally want thumbnail creation done inside the
request/response cycle, I've designed this to follow an eventually consistent
model.  This means that the user can `POST` an image, but then must `GET` their
image list afterward, potentially after a short time while the background
process generates the thumbnail.


### Expiring Links

The spec didn't make any mention of what the link would be pointing to, so I
made the assumption that it was meant to target the original image.
Additionally, it wasn't clear if there should be rules attached to the expiring
URL around who should have the rights to see it.  As it's common to use such
URLs to share with the public, the view has no restrictions on expiring URLs.

I should also point out that this requirement makes very little sense to
implement in your application as it forces static files to be loaded through
the app, gobbling up bandwidth and CPU that could be used for better/cheaper
things.  Typically for something like this, you'd just lean on something like
AWS S3's built-in image expiry system.
