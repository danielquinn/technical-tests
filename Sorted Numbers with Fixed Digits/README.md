# Sorted Numbers with Fixed Digits

Generate a list of numbers that are `n` digits long according to the following
rules:

* Numbers may not have any leading zeroes
* The numbers in each number must be in ascending order.  Ie. `123` and `124`
  are alright, but `132` is not.
