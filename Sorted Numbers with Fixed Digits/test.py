#!/bin/env python

def test(number_of_digits: int):

    started = 10 ** (number_of_digits - 1)
    stopped = 10 ** number_of_digits

    for i in range(started, stopped):
        string_version = str(i)
        # Probably not the most performant test, but my math skills are sub-par
        if "".join(sorted(string_version)) == string_version:
            yield i


assert list(test(1)) == [1, 2, 3, 4, 5, 6, 7, 8, 9]
assert list(test(2)) == [11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 24, 25, 26, 27, 28, 29, 33, 34, 35, 36, 37, 38, 39, 44, 45, 46, 47, 48, 49, 55, 56, 57, 58, 59, 66, 67, 68, 69, 77, 78, 79, 88, 89, 99]
