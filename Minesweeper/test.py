from random import randint


class Minesweeper:
    def __init__(self, columns: int, rows: int, bombs=10):
        self.columns = columns
        self.rows = rows
        self.bomb_count = bombs
        self.board = self.__generate_board(rows, columns)

    @staticmethod
    def __generate_board(rows: int, columns: int) -> list:
        r = []
        for _ in range(rows):
            row = []
            for __ in range(columns):
                row.append(" ")
            r.append(row)
        return r

    def __get_available_coordinate(self):
        while True:
            row = randint(0, self.rows - 1)
            column = randint(0, self.columns - 1)
            if not self.__check_has_bomb(row, column):
                return row, column

    def __get_bomb_count(self, row: int, column: int):

        col_max = self.columns - 1
        row_max = self.rows - 1

        r = 0

        # Above
        if row - 1 >= 0:
            if column - 1 >= 0:
                if self.__check_has_bomb(row - 1, column - 1):
                    r += 1
            if self.__check_has_bomb(row - 1, column):
                r += 1
            if column + 1 <= col_max:
                if self.__check_has_bomb(row - 1, column + 1):
                    r += 1

        # Left
        if column - 1 >= 0:
            if self.__check_has_bomb(row, column - 1):
                r += 1

        # Right
        if column + 1 <= col_max:
            if self.__check_has_bomb(row, column + 1):
                r += 1

        # Below
        if row + 1 <= row_max:
            if column - 1 >= 0:
                if self.__check_has_bomb(row + 1, column - 1):
                    r += 1
            if row + 1 <= row_max:
                if self.__check_has_bomb(row + 1, column):
                    r += 1
            if column + 1 <= col_max:
                if self.__check_has_bomb(row + 1, column + 1):
                    r += 1

        return r

    def __check_has_bomb(self, row: int, column: int):
        return self.board[row][column] == "x"

    def set_bombs(self):
        for bomb in range(self.bomb_count):
            row, column = self.__get_available_coordinate()
            self.board[row][column] = "x"

    def set_flags(self):

        for row_index, row in enumerate(self.board):

            for column_index, column in enumerate(row):

                if self.__check_has_bomb(row_index, column_index):
                    continue

                total_flags = self.__get_bomb_count(row_index, column_index)

                if not total_flags:
                    continue

                self.board[row_index][column_index] = total_flags

    def render(self):
        first_row = " ".join([chr(_) for _ in range(65, (65 + self.columns))])
        print(f"   {first_row}")
        for index, row in enumerate(self.board, start=1):
            print(f"{index:>2}", end=" ")
            for cell in row:
                print(cell, end=" ")
            print("")

    def play(self):
        self.set_bombs()
        self.set_flags()
        self.render()


if __name__ == "__main__":
    Minesweeper(26, 26, 20).play()
