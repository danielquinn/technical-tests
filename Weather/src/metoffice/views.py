import json
from django.views.generic import TemplateView

from weather.models import Region


class VariablesJsView(TemplateView):
    """
    In an attempt to keep the Javascript DRY, stuff that we define in
    Pythonland that we also need in Javascriptland is funnelled through here.
    """

    template_name = "metoffice/variables.js"
    content_type = "text/javascript"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["regions"] = json.dumps(
            list(Region.objects.values_list("name", flat=True)),
            separators=(",", ":")
        )
        return context
