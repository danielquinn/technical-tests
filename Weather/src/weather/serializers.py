from rest_framework import serializers

from .models import Metric


class MetricSerializer(serializers.ModelSerializer):

    region = serializers.CharField(source="region.name", read_only=True)

    temp_min = serializers.DecimalField(
        max_digits=5,
        decimal_places=2,
        coerce_to_string=False,
        read_only=True
    )
    temp_max = serializers.DecimalField(
        max_digits=5,
        decimal_places=2,
        coerce_to_string=False,
        read_only=True
    )
    temp_mean = serializers.DecimalField(
        max_digits=5,
        decimal_places=2,
        coerce_to_string=False,
        read_only=True
    )
    sunshine = serializers.DecimalField(
        max_digits=5,
        decimal_places=1,
        coerce_to_string=False,
        read_only=True
    )
    rainfall = serializers.DecimalField(
        max_digits=5,
        decimal_places=1,
        coerce_to_string=False,
        read_only=True
    )

    class Meta:
        model = Metric
        fields = (
            "region",
            "date",
            "temp_min",
            "temp_max",
            "temp_mean",
            "sunshine",
            "rainfall",
        )

    def __init__(self, *args, **kwargs):

        self._field_limiters = []

        field_limiters = kwargs.pop("field_limiters", None)
        if field_limiters:
            for field in self.Meta.fields:
                if field in field_limiters:
                    self._field_limiters.append(field)

        super().__init__(*args, **kwargs)

    def get_fields(self):

        r = super().get_fields()

        if not self._field_limiters:
            return r

        to_delete = []
        for field in r:
            if field not in self._field_limiters:
                to_delete.append(field)

        for field in to_delete:
            del(r[field])

        return r
