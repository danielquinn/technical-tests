import datetime
import re

import requests
from django.core.management import BaseCommand
from django.db import IntegrityError
from django.utils import timezone

from metoffice.logging import LogMixin

from ...models import Metric, Region


class Command(LogMixin, BaseCommand):

    # The Met Office uses the nuttiest data format I've seen in a long time.
    # This was the simplest way I could figure out how to parse it.
    REGEX = re.compile("^(\d{4}) {2,4}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*) {2,7}([\-\d.]*)")  # NOQA: E501

    SOURCE_TEMPLATE = "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/datasets/{type}/date/{region}.txt"  # NOQA: E501
    SOURCE_TYPES = {
        "Tmax": "temp_max",
        "Tmin": "temp_min",
        "Tmean": "temp_mean",
        "Sunshine": "sunshine",
        "Rainfall": "rainfall"
    }

    def handle(self, **options):

        for region in Region.objects.all():

            self._pre_create_rows(region)

            for external_type, internal_type in self.SOURCE_TYPES.items():
                self._consume_url(
                    region,
                    internal_type,
                    self.SOURCE_TEMPLATE.format(
                        type=external_type,
                        region=region.name
                    )
                )

    def _pre_create_rows(self, region):

        for year in range(1910, timezone.now().year + 1):
            for month in range(1, 13):
                try:
                    Metric.objects.create(
                        region=region,
                        date=timezone.make_aware(
                            datetime.datetime(year, month, 1)
                        )
                    )
                except IntegrityError:
                    continue
                self.logger.info(f"Created blank record for {region}:{year}")

    def _consume_url(self, region, type_, url):

        self.logger.info(f"Fetching {region}:{type_}")

        response = requests.get(url)

        if not response.status_code == 200:
            self.logger.error("Fetch failed: %s", url)
            return

        for line in response.iter_lines():

            line = str(line, "utf-8").strip()
            m = self.REGEX.match(line)
            if not m:
                self.logger.warning("Ignored data: %s", line)
                continue

            self.consume_line(region, type_, m.groups())

    def consume_line(self, region, type_, fields):

        for month, data in enumerate(fields[1:13], 1):

            year = int(fields[0])
            date = timezone.make_aware(datetime.datetime(year, month, 1))

            if not data:
                self.logger.warning(f"No data for {type_}:{year}/{month:02}")
                continue

            Metric.objects.filter(
                region=region, date=date).update(**{type_: data})
