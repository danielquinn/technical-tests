from django.db import models


class Region(models.Model):
    name = models.CharField(unique=True, max_length=64)

    def __str__(self):
        return self.name


class Metric(models.Model):

    class Meta:
        unique_together = ("region", "date")

    region = models.ForeignKey(
        Region, related_name="metrics", on_delete=models.CASCADE)

    date = models.DateField(db_index=True)
    temp_min = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    temp_max = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    temp_mean = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    sunshine = models.DecimalField(max_digits=5, decimal_places=1, null=True)
    rainfall = models.DecimalField(max_digits=5, decimal_places=1, null=True)

    def __str__(self):
        return f"{self.region}: {self.date}"
