import json
from datetime import datetime
from django.test import TestCase, RequestFactory
from django.urls import reverse
from django.utils import timezone

from ..models import Metric, Region
from ..views import MetricListView


class IntegrationTestCase(TestCase):
    
    fixtures = ("regions.json",)

    def setUp(self):

        self.factory = RequestFactory()

        self.url = reverse("metrics")

        eng = Region.objects.get(name="England")
        wal = Region.objects.get(name="Wales")
        sco = Region.objects.get(name="Scotland")

        Metric.objects.bulk_create((
            Metric(date=timezone.make_aware(datetime(2018, 1, 1)), region=eng),
            Metric(date=timezone.make_aware(datetime(2018, 2, 1)), region=eng),
            Metric(date=timezone.make_aware(datetime(2018, 3, 1)), region=eng),
            Metric(date=timezone.make_aware(datetime(2018, 4, 1)), region=eng),
            Metric(date=timezone.make_aware(datetime(2018, 1, 1)), region=wal),
            Metric(date=timezone.make_aware(datetime(2018, 2, 1)), region=wal),
            Metric(date=timezone.make_aware(datetime(2018, 3, 1)), region=wal),
            Metric(date=timezone.make_aware(datetime(2018, 4, 1)), region=wal),
            Metric(date=timezone.make_aware(datetime(2018, 1, 1)), region=sco),
            Metric(date=timezone.make_aware(datetime(2018, 2, 1)), region=sco),
            Metric(date=timezone.make_aware(datetime(2018, 3, 1)), region=sco),
            Metric(date=timezone.make_aware(datetime(2018, 4, 1)), region=sco),
        ))

        self.england = eng
        self.wales = wal
        self.scotland = sco

    def test_basic(self):
        request = self.factory.get("")
        response = MetricListView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.render().content)), 12)

    def test_date_filter(self):

        response = self.client.get(f"{self.url}?date=2018-01-01")
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.render().content)
        self.assertEqual(len(content), 3)
        self.assertEqual(set([p["date"] for p in content]), {"2018-01-01"})

        response = self.client.get(f"{self.url}?date__gt=2018-02-01")
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.render().content)
        self.assertEqual(len(content), 6)
        self.assertEqual(
            set([p["date"] for p in content]), {"2018-03-01", "2018-04-01"})

        response = self.client.get(f"{self.url}?date__lte=2018-02-01")
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.render().content)
        self.assertEqual(len(content), 6)
        self.assertEqual(
            set([p["date"] for p in content]), {"2018-01-01", "2018-02-01"})

    def test_region_filter(self):

        response = self.client.get(f"{self.url}?region__name=Scotland")
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.render().content)
        self.assertEqual(len(content), 4)
        self.assertEqual(set([p["region"] for p in content]), {"Scotland"})

    def test_both_filters(self):

        response = self.client.get(
            f"{self.url}?region__name=Scotland&date=2018-02-01")

        self.assertEqual(response.status_code, 200)
        content = json.loads(response.render().content)
        self.assertEqual(len(content), 1)
        self.assertEqual(
            set([(p["region"], p["date"]) for p in content]),
            {("Scotland", "2018-02-01")}
        )

    def test_field_limiters(self):

        response = self.client.get(f"{self.url}?fields=temp_mean,sunshine")

        self.assertEqual(response.status_code, 200)
        content = json.loads(response.render().content)
        self.assertEqual(len(content), 12)
        for point in content:
            for field in ("temp_mean", "sunshine"):
                self.assertIn(field, point)
            for field in ("temp_min", "temp_max", "region", "rainfall"):
                self.assertNotIn(field, point)
