from django.test import TestCase

from ..serializers import MetricSerializer


class RateLogSerializerTestCase(TestCase):

    def test___init___without_fields(self):
        serializer = MetricSerializer()
        self.assertEqual(serializer._field_limiters, [])

    def test___init___with_good_fields(self):
        serializer = MetricSerializer(field_limiters=["region", "date"])
        self.assertEqual(serializer._field_limiters, ["region", "date"])

    def test___init___with_naughty_fields(self):
        serializer = MetricSerializer(field_limiters=["test", "tset"])
        self.assertEqual(serializer._field_limiters, [])

    def test_get_fields_without_restrictions(self):
        serializer = MetricSerializer()
        self.assertEqual(
            set(serializer.get_fields().keys()),
            {
                "region",
                "date",
                "temp_min",
                "temp_max",
                "temp_mean",
                "sunshine",
                "rainfall",
            }
        )

    def test_get_fields_with_restrictions(self):
        serializer = MetricSerializer(field_limiters=["region", "date"])
        self.assertEqual(
            set(serializer.get_fields().keys()),
            {"region", "date"}
        )
