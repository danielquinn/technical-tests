from datetime import datetime
from decimal import Decimal
from unittest import mock

import freezegun
from django.test import TestCase
from django.utils import timezone

from ..management.commands.pull_weather import Command
from ..models import Metric, Region


class PullWeatherTestCase(TestCase):

    fixtures = ("regions.json",)

    def test_regex(self):
        lines = {
            "Year    JAN    FEB    MAR    APR    MAY    JUN    JUL    AUG    SEP    OCT    NOV    DEC     WIN    SPR    SUM    AUT     ANN": (False, ()),
            "1910  146.2  173.6   53.3  109.7   75.3  135.6  120.5  190.5   13.5  130.3  192.3  227.7     ---  238.3  446.7  336.0  1568.5": (True, ('1910', '146.2', '173.6', '53.3', '109.7', '75.3', '135.6', '120.5', '190.5', '13.5', '130.3', '192.3', '227.7', '---', '238.3', '446.7', '336.0', '1568.5')),
            "1910    5.4    6.8    9.1    9.8   14.4   17.7   17.0   17.7   15.6  -12.8    5.9    7.7     ---  11.08  17.48  11.44   11.68": (True, ('1910', '5.4', '6.8', '9.1', '9.8', '14.4', '17.7', '17.0', '17.7', '15.6', '-12.8', '5.9', '7.7', '---', '11.08', '17.48', '11.44', '11.68')),
            "2017    6.7    7.9   10.9   12.2   16.8   18.4   19.3   18.5   16.1   14.3    8.8    7.0    7.85  13.29  18.75  13.08   13.10": (True, ('2017', '6.7', '7.9', '10.9', '12.2', '16.8', '18.4', '19.3', '18.5', '16.1', '14.3', '8.8', '7.0', '7.85', '13.29', '18.75', '13.08', '13.10')),
            "2018    6.7    5.4                                                                          6.39": (True, ('2018', '6.7', '5.4', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '6.39')),
            "2018  188.9   79.9                                                                         445.9": (True, ('2018', '188.9', '79.9', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '445.9'))
        }
        for data_in, (accepted, data_out) in lines.items():
            m = Command.REGEX.match(data_in)
            if accepted:
                self.assertTrue(m)
                self.assertEqual(len(m.groups()), 18)
                self.assertEqual(m.groups(), data_out)
            else:
                self.assertFalse(m)

    @freezegun.freeze_time("1919-01-01")
    def test__pre_create_rows(self):

        region = Region.objects.first()

        self.assertEqual(Metric.objects.count(), 0)

        Command()._pre_create_rows(region)

        metrics = Metric.objects.order_by("date")
        self.assertEqual(metrics.count(), 120)

        first = metrics.first()
        self.assertEqual(first.date.year, 1910)
        self.assertEqual(first.date.month, 1)

        last = metrics.order_by("-date").first()
        self.assertEqual(last.date.year, 1919)
        self.assertEqual(last.date.month, 12)

    @mock.patch("weather.management.commands.pull_weather.requests.get")
    def test__consume_url_404(self, m):
        mocked_lines = mock.Mock()
        m.return_value = mock.Mock(status_code=404, iter_liens=mocked_lines)
        Command()._consume_url(None, None, None)
        self.assertEqual(mocked_lines.call_count, 0)

    @mock.patch("weather.management.commands.pull_weather.requests.get")
    def test__consume_url_200(self, m):
        mocked_lines = mock.Mock(return_value=(
            b"Year    JAN    FEB    MAR    APR    MAY    JUN    JUL    AUG    SEP    OCT    NOV    DEC     WIN    SPR    SUM    AUT     ANN",
            b"1910  146.2  173.6   53.3  109.7   75.3  135.6  120.5  190.5   13.5  130.3  192.3  227.7     ---  238.3  446.7  336.0  1568.5",
            b"1910    5.4    6.8    9.1    9.8   14.4   17.7   17.0   17.7   15.6   12.8    5.9    7.7     ---  11.08  17.48  11.44   11.68",
            b"2017    6.7    7.9   10.9   12.2   16.8   18.4   19.3   18.5   16.1   14.3    8.8    7.0    7.85  13.29  18.75  13.08   13.10",
            b"2018    6.7    5.4                                                                          6.39",
            b"2018  188.9   79.9                                                                         445.9",
        ))
        m.return_value = mock.Mock(status_code=200, iter_lines=mocked_lines)

        command = Command()
        with mock.patch.object(command, "consume_line") as m_cl:
            command._consume_url(None, None, None)
            self.assertEqual(mocked_lines.call_count, 1)
            self.assertEqual(m_cl.call_count, 5)

    def test_consume_line(self):

        region = Region.objects.first()
        type_ = "sunshine"
        fields = ('2018', '188.9', '79.9', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '445.9')

        self.assertEqual(Metric.objects.count(), 0)
        for month in range(1, 13):
            Metric.objects.create(
                region=region,
                date=timezone.make_aware(datetime(2018, month, 1))
            )

        self.assertEqual(Metric.objects.count(), 12)

        Command().consume_line(region, type_, fields)
        self.assertEqual(
            tuple(Metric.objects.order_by("date").values_list("sunshine", flat=True)),
            (
                Decimal("188.9"),
                Decimal('79.9'),
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
            )
        )
