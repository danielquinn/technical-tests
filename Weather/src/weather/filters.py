from django_filters import rest_framework as filters

from .models import Metric


class MetricFilterSet(filters.FilterSet):

    region__name = filters.CharFilter(
        "region__name", lookup_expr="exact", required=True)
    date__lt = filters.DateFilter("date", lookup_expr="lt")
    date__lte = filters.DateFilter("date", lookup_expr="lte")
    date__gt = filters.DateFilter("date", lookup_expr="gt")
    date__gte = filters.DateFilter("date", lookup_expr="gte")
    date = filters.DateFilter("date", lookup_expr="exact")

    class Meta:
        model = Metric
        fields = (
            "region__name",
            "date__lt",
            "date__lte",
            "date__gt",
            "date__gte",
            "date"
        )
