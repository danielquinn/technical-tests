#!/usr/bin/env python3


def test(i, fizz, buzz):
    if not i % fizz:
        if not i % buzz:
            return "FizzBuzz"
        return "Fizz"
    if not i % buzz:
        return "Buzz"
    return i


for i in range(1, 101):
    print(test(i, 3, 5))

