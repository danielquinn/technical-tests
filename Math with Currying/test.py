def gen_number(integer):
    def w00t(fn=None):
        if fn:
            return fn(integer)
        return integer
    return w00t


def gen_operator(operator):
    def w00t(integer):
        return lambda other_integer : operator(other_integer, integer)
    return w00t


zero = gen_number(0)
one = gen_number(1)
two = gen_number(2)
three = gen_number(3)
four = gen_number(4)
five = gen_number(5)
six = gen_number(6)
seven = gen_number(7)
eight = gen_number(8)
nine = gen_number(9)

plus = gen_operator(int.__add__)
minus = gen_operator(int.__sub__)
times = gen_operator(int.__mul__)
divided_by = gen_operator(int.__truediv__)
