# Connect Z

It's like [Connect Four](https://en.wikipedia.org/wiki/Connect_Four), but with variable width, height, and win condition values.


## Input format

```
n n n
n
n
n
n
```

The first row represents the `width`, `height`, and `win_condition` values, whereas each following row is the column into which a piece is added.

