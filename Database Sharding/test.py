#!/usr/bin/env python3

import json
import sqlite3

from random import randint
from uuid import uuid4

#
# It should be noted that the vast majority of this code can be avoided by
# simply picking a reasonably future-proof value for sharding and running with
# it:
#
#     def _get_table(self):
#         return self.pk[-1]
#
# That'll spread the comments across 16 tables evenly and without 95% of this
# code.  If you want more, use [-2:] and get 256 tables, *and* as a bonus, you
# get a more uniform balance across tables.
#
# The specs called for a rebalancing from 4 to 6 though, so now we have to get
# complicated.
#


class Database(object):
    """
    A singleton to handle the database connectivity
    """

    DB_LOCATION = ":memory:"  # Use a real file path for persistence

    debugging = False
    connection = sqlite3.connect(DB_LOCATION)
    cursor = connection.cursor()

    @classmethod
    def query(cls, query, parameters=()):
        """
        Shortcut to make my code readable and a little easier to debug.
        """

        if cls.debugging:
            print(query)
            print(parameters)

        return Database.cursor.execute(query, parameters)


class Comment(object):

    class DoesNotExist(Exception):
        pass

    # Strictly speaking this isn't the sort of value that should be an instance
    # value, since you don't typically change the storage engine on-the-fly.
    # It's because of this that I just couldn't bring myself to put in __init__
    # like an instance variable.  Instead, it lives here as a constant... even
    # though we modify this "constant" for the purposes of this exercise.
    BALANCERS = 6

    def __init__(self, pk=None, text=None):
        self.pk = pk
        self.text = text

    @classmethod
    def get_object_by_id(cls, pk):
        try:
            return cls(
                pk=pk,
                text=Database.query("SELECT text FROM {} WHERE id = ?".format(
                    cls.get_table_for_id(pk)
                ), (pk,)).fetchone()[0]
            )
        except:
            raise cls.DoesNotExist(
                "Looks like there's nothing here by that id"
            )

    def save(self, commit=True):

        Database.query(
            "INSERT INTO {} VALUES (?, ?, ?)".format(self._get_table()),
            (self.pk, int(self.pk[0], 16), self.text)
        )

        if commit:
            Database.connection.commit()

    def delete(self):
        Database.query(
            "DELETE FROM {} WHERE id = '{}'".format(self._get_table(), self.pk)
        )

    def _get_table(self):
        """
        An id is a UUID with an essentially random hex value as the first
        character.  We can translate this into decimal and then modulo it
        against the number of balancers to get the table name.
        """
        return self.get_table_for_id(self.pk)

    @classmethod
    def get_table_for_id(cls, pk):
        """
        An id is a UUID with an essentially random hex value as the first
        character.  We can translate this into decimal and then modulo it
        against the number of balancers to get the table name.
        """
        return "comment{}".format((int(pk[0], 16) % cls.BALANCERS))

    @classmethod
    def create_tables_if_needed(cls, shards):
        """
        Creates tables if they don't exist already.
        """

        for i in range(0, shards):
            try:
                Database.query("""
                    CREATE TABLE comment{} (
                        id text UNIQUE NOT NULL,
                        balancer int NOT NULL,
                        text text NOT NULL
                    )
                """.format(i))
            except sqlite3.OperationalError:
                pass  # Better to ask forgivness than permission

    @classmethod
    def create(cls, text, commit=True):
        cls(pk=str(uuid4()), text=text).save(commit=commit)

    @classmethod
    def random_fill(cls, count):

        with open("/tmp/words.json") as f:
            words = json.load(f)
        length = len(words) - 1

        # Generate and save <count> comments with a text string 50 words long
        for i in range(0, count):
            print(
                "{}Backfilling comments: {:<7}".format("\b" * 30, i + 1),
                end=""
            )
            cls.create(
                " ".join([words[randint(0, length)] for __ in range(0, 50)]),
                commit=False
            )
        print("\n")

        Database.connection.commit()

    @classmethod
    def get_counts(cls):
        r = "Current Comment Count per Table\n{}\n".format("-" * 79)
        total = 0
        for table in cls.get_tables(cls.BALANCERS):
            Database.query("SELECT COUNT(*) FROM {}".format(table))
            table_total = Database.cursor.fetchone()[0]
            r += "{}: {}\n".format(table, table_total)
            total += table_total
        r += "\nTotal: {}\n".format(total)
        return r

    @classmethod
    def get_tables(cls, balancers):
        return ["comment{}".format(i) for i in range(0, balancers)]

    @classmethod
    def rebalance(cls, old_balance, new_balance):

        cls.BALANCERS = new_balance

        keys = range(0, 16)

        old_map = {}
        for i in keys:
            old_map.setdefault(i % old_balance, []).append(i)

        new_map = {}
        for i in keys:
            new_map.setdefault(i % new_balance, []).append(i)

        cls.create_tables_if_needed(new_balance)

        for shard in old_map.keys():

            try:
                to_move = set(old_map[shard]) - set(new_map[shard])
            except KeyError:  # We're shrinking
                to_move = keys

            for mover in to_move:

                Database.query("""
                    INSERT INTO {table1} (
                        id,
                        balancer,
                        text
                    )
                    SELECT
                        id,
                        balancer,
                        text
                    FROM
                        {table2}
                    WHERE
                        balancer = ?
                """.format(
                    table1="comment{}".format(mover % new_balance),
                    table2="comment{}".format(shard),
                ), (mover,))

                Database.query("""
                    DELETE
                    FROM
                        {}
                    WHERE
                        balancer = ?
                """.format("comment{}".format(shard)), (mover,))

        Database.connection.commit()

    @classmethod
    def test_balance(cls):

        table_map = {}
        for i in range(0, 16):
            table_map.setdefault(i % cls.BALANCERS, []).append(i)

        for shard, table_numbers in table_map.items():

            table = "comment{}".format(shard)

            total = Database.query(
                "SELECT COUNT(*) FROM {}".format(table)).fetchone()[0]

            filtered_total = Database.query(
                "SELECT COUNT(*) FROM {} WHERE balancer IN ({})".format(
                    table,
                    ", ".join([str(i) for i in table_numbers])
                ),
            ).fetchone()[0]

            assert(total == filtered_total)


if __name__ == "__main__":

    Comment.create_tables_if_needed(Comment.BALANCERS)

    Comment.random_fill(10000)
    print("Get Object Test\n{}".format("-" * 79))
    comment = Comment.get_object_by_id(
        Database.query("SELECT id FROM comment0 LIMIT 1").fetchone()[0])
    print("{}: {}\n".format(comment.pk, comment.text))

    print(Comment.get_counts())
    Comment.test_balance()

    print("Rebalancing to 6...\n")
    Comment.rebalance(4, 6)
    print(Comment.get_counts())
    Comment.test_balance()

    print("Rebalancing to 2...\n")
    Comment.rebalance(6, 2)
    print(Comment.get_counts())
    Comment.test_balance()

    print("Rebalancing to 16...\n")
    Comment.rebalance(2, 16)
    print(Comment.get_counts())
    Comment.test_balance()
