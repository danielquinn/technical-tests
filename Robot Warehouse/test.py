#!/usr/bin/env python3


class MovingException(Exception):
    pass


class CrateException(Exception):
    pass


class Warehouse(object):

    NORTH = "N"
    EAST = "E"
    SOUTH = "S"
    WEST = "W"

    DIRECTIONS = (NORTH, EAST, SOUTH, WEST)

    def __init__(self, locations=()):
        self.crate_locations = set(locations)

    def violates_board(self, position, direction):

        if direction == self.NORTH and position[1] > 8:
            return True
        elif direction == self.EAST and position[0] > 8:
            return True
        elif direction == self.SOUTH and position[1] < 1:
            return True
        elif direction == self.WEST and position[0] < 1:
            return True

        return False

    def has_crate(self, position):
        if position in self.crate_locations:
            return True
        return False

    def remove_crate(self, position):
        if not self.has_crate(position):
            raise CrateException("There's nothing here")
        self.crate_locations.remove(position)

    def add_crate(self, position):
        if self.has_crate(position):
            raise CrateException("Too many crates :-(")
        self.crate_locations.add(position)


class Robot(object):

    def __init__(self, warehouse, position=(0, 0), is_carrying=False):
        self.warehouse = warehouse
        self.position = position
        self.is_carrying = is_carrying

    def move(self, orders):

        for direction in orders:

            if direction not in self.warehouse.DIRECTIONS:
                raise MovingException("That direction is invalid")

            if self.warehouse.violates_board(self.position, direction):
                raise MovingException(
                    "I'm sorry Dave, I'm afraid I can't do that")

            if direction == self.warehouse.NORTH:
                self.position = self.position[0], self.position[1] + 1
            elif direction == self.warehouse.EAST:
                self.position = self.position[0] + 1, self.position[1]
            elif direction == self.warehouse.SOUTH:
                self.position = self.position[0], self.position[1] - 1
            elif direction == self.warehouse.WEST:
                self.position = self.position[0] - 1, self.position[1]

        print("New position: {}".format(self.position))

    def pickup_crate(self):
        if self.is_carrying:
            raise CrateException("I can't carry more than one crate")
        self.warehouse.remove_crate(self.position)
        self.is_carrying = True

    def drop_crate(self):
        if not self.is_carrying:
            raise CrateException("Dude, I've got nothing")
        self.warehouse.add_crate(self.position)
        self.is_carrying = False


def tests():

    warehouse = Warehouse(locations=[(0, 0), (4, 4)])
    robot = Robot(warehouse)

    print("pass")
    robot.move([Warehouse.NORTH])

    print("pass")
    robot.move([Warehouse.NORTH, Warehouse.EAST])

    try:
        robot.move([Warehouse.WEST, Warehouse.WEST])
    except MovingException:
        print("It failed and that's ok")

    robot.move([
        Warehouse.NORTH,
        Warehouse.NORTH,
        Warehouse.EAST,
        Warehouse.EAST,
        Warehouse.EAST,
        Warehouse.EAST,
    ])
    robot.pickup_crate()
    print(robot.is_carrying)
    robot.drop_crate()
    print(robot.is_carrying)
    robot.pickup_crate()
    print(robot.is_carrying)
    robot.move([
        Warehouse.SOUTH,
        Warehouse.SOUTH,
        Warehouse.SOUTH,
        Warehouse.SOUTH,
        Warehouse.WEST,
        Warehouse.WEST,
        Warehouse.WEST,
        Warehouse.WEST,
    ])
    try:
        robot.drop_crate()
    except CrateException:
        print("Happy fail")
    robot.move([Warehouse.NORTH])
    robot.drop_crate()


if __name__ == '__main__':
    tests()
