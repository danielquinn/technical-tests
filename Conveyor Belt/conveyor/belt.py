import random
import time

from .constants import COMPONENT_A, COMPONENT_B, PRODUCT
from .slot import Slot
from .worker import Worker


class Belt:
    """
    The main class that does most of the work.  This is where our running loop
    lives, and it's also where we handle events.  It's sort of a weird setup,
    I know: the belt is effectively telling workers to do things.  However,
    this seemed a lot simpler than creating a controller that monitored the
    belt and notified the workers itself.
    """

    def __init__(self, length, loop_time=0, loop_total=100):

        self.slots = [Slot() for _ in range(length)]
        self.workers = [(Worker(), Worker()) for _ in range(length)]

        # Purely cosmetic.  You can adjust this to make the "animation" a
        # little easier on the eyes or just set it to 0 for performance.
        self.loop_time = loop_time

        # Part of the requirements, but making this number bigger makes the
        # animation a little more interesting.
        self.loop_total = loop_total

        self.tally = {
            "produced": 0,
            "wasted": 0
        }

    def run(self):

        for _ in range(self.loop_total):
            self.on_tick()
            if self.loop_time:
                time.sleep(self.loop_time)

        print("Produced: {}\nWaste: {}".format(
            self.tally["produced"],
            self.tally["wasted"]
        ))

    def advance(self):
        """
        This is kind of an ugly trick, but it makes everything else so much
        easier that it only made sense.  Rather than actually moving objects
        through the belt from slot to slot to slot, we instead pop off the last
        element and add one to the start.  This resets all the indexes in the
        list saving the hassle of moving everything.

        This is where we tally the productivity and use random.randint() to
        decide if a new component is coming down the pipe.

        I'm assuming here that we don't want terribly high resolution on what
        was produced/wasted.  Currently, I'm only tracking whether a product
        was product or if a component (regardless of type) was wasted, but we
        could do all sorts of fun things like have the workers keep track of
        what they produce (or even when) and tally them up separately.
        """

        slot = self.slots.pop()
        if slot.holding == PRODUCT:
            self.tally["produced"] += 1
        elif slot.holding in (COMPONENT_A, COMPONENT_B):
            self.tally["wasted"] += 1

        self.slots.insert(0, Slot())
        if random.randint(1, 2) % 2:
            self._add_component()

    def on_tick(self):
        """
        Executed on every loop, this is where all the magic happens.  We help
        the workers keep time, then advance the belt, then fire events based
        on what's in each slot.  Finally, we render out the story so far.
        """

        for pair in self.workers:
            for worker in pair:
                worker.tick()

        self.advance()

        for index, slot in enumerate(self.slots):
            if not slot:  # Space free
                self.on_slot_free(index)
            else:
                self.on_component_available(index)

        self._render_state()

    def on_slot_free(self, index):
        """
        If there's nothing in a slot, let the workers know that there's an
        opportunity to place something there.  Exit early so two workers don't
        attempt to place something into the same slot.
        :param index: int The index of the slot
        """
        for worker in self.workers[index]:
            if worker.has_finished_component:
                self.slots[index].push(worker.return_finished_product())
                return

    def on_component_available(self, index):
        """
        If there's a component available in a slot, get the workers on that
        slot to pay attention.  Bail as soon as a worker acquires a component
        since after that there's nothing to acquire.
        :param index: int The index of the slot.
        """
        for worker in self.workers[index]:
            if worker.wants(self.slots[index].holding):
                worker.acquire_component(self.slots[index].pop())
                return

    def _add_component(self):
        """
        Generates a random component to add to the belt.  It will appear in the
        first slot.
        """
        self.slots[0].push(random.choice((COMPONENT_A, COMPONENT_B)))

    def _render_state(self):
        """
        Not part of the test, but this really helps visualise just what's going
        on.  This attempts to draw a text-based representation of the conveyor
        belt and workers, including what each worker is currently holding.
        """
        print("\n" * 120)  # Poor man's "clear" to make things look animated
        print("    {}\n{}\n-> | {} | ->\n{}\n    {}\n\n".format(
            " ".join([str(w[0]) for w in self.workers]),
            "--" * len(self.slots) * 3,
            " | ".join([str(s) for s in self.slots]),
            "--" * len(self.slots) * 3,
            " ".join([str(w[1]) for w in self.workers])
        ))
