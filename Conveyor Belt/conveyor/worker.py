from .constants import COMPONENT_A, COMPONENT_B, PRODUCT


class Worker:
    """
    Workers are pretty simple: they have a limited number of ``holding`` slots
    and a single desire to make products from components.  This desire is
    expressed through ``.wants()``, and the actual production is done in
    ``.tick()``.

    Assumption: the spec dictates that workers only have two hands, so this
    code is a little rigid around that aspect.  If we want to allow for a
    variable number of hands, some of these methods, , like ``__init__()`` and
    ``return_finished_product()`` will have to be more complex.
    """

    CONSTRUCTION_TIME = 4  # Belt movement units

    def __init__(self, left=None, right=None):
        self.holding = [left, right]
        self.construction_time_remaining = 0

    def __str__(self):
        """
        Renders what we've got as three characters:

          ``[hand1]_[hand2]``

        Examples:
          ``A__``
          ``B__``
          ``A_B``
          ``P__``
          ``A_P``
        """
        return "_".join(str(_ or "_") for _ in self.holding)

    @property
    def has_free_hand(self):
        """
        Return True if ``self.holding`` has a free slot.
        """
        return None in self.holding

    @property
    def has_finished_component(self):
        return PRODUCT in self.holding

    def wants(self, obj):
        """
        Workers are very specific about their needs:
          * Only one of any item type
          * Only two items at any given time
          * We only ever want components
        This returns True if the component in question satisfies these needs.
        :param obj: str Either a component or a product
        """
        if self.has_free_hand:
            if obj in (COMPONENT_A, COMPONENT_B):
                if obj not in self.holding:
                    return True
        return False

    def acquire_component(self, component):
        """
        Put a component in one of our free hands.
        :param component: str One of the component types
        """
        self.holding[self.holding.index(None)] = component
        if COMPONENT_A in self.holding and COMPONENT_B in self.holding:
            self.construction_time_remaining = self.CONSTRUCTION_TIME

    def return_finished_product(self):
        """
        Take the finished product out of our hand and return it.
        """
        product = self.holding.pop(self.holding.index(PRODUCT))
        self.holding.append(None)
        return product

    def tick(self):
        """
        How the worker keeps track of time.  Mostly this is just here so the
        worker knows how long it's been since it's been holding two components
        so we can declare them a product.
        """
        if self.construction_time_remaining:
            self.construction_time_remaining -= 1
            if self.construction_time_remaining == 0:
                self.holding = [PRODUCT, None]
