class Marine:

    def __init__(self, damage, armour):
        self.damage = damage
        self.armour = armour


class MarineDecorator:

    def __init__(self, marine):
        self._marine = marine

    def __getattr__(self, item):
        return getattr(self._marine, item)


class MarineWeaponUpgrade(MarineDecorator):

    @property
    def damage(self):
        return self._marine.damage + 1


class MarineArmourUpgrade(MarineDecorator):

    @property
    def armour(self):
        return self._marine.armour + 1

